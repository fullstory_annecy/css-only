const controller = new ScrollMagic.Controller({
    refreshInterval: 1000
});

const createScroll = (id) => {

    let scene = new ScrollMagic.Scene({
        triggerHook: .5,
        triggerElement: id
    })
        .setClassToggle(id,'active')
        .addTo(controller);
};

createScroll('#iphone');
createScroll('#apple-watch');
createScroll('#iphone-xr');